#import <stdio>

int Fibonacci(int n)
{
   if ( n == 0 ) {
      return 0;
   }
   if ( n == 1) {
      return 1;
   }
   int tmp = Fibonacci(n - 1) + Fibonacci(n - 2);
   return tmp;
}

int main() {
    int x = 0;
    while(x <= 10) {
        printf("Fibonacci of %i: %i\n", x, Fibonacci(x));
    }

    return 0;
}
