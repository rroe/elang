#import <stdio>
#import <math>
#import <time>
#import <stdlib>

// Returns a number between 0 and max (non-inclusive)
int randInt(int max) {
  srand(time(NULL));
  int out = rand();
  return (out % max);
}

/*
  This is the main method
*/
int main() {
  int x = randInt(10) + 1;
  printf("Random number between 1 and 10: %i\n", x);
  return 0;
}
