#include <stdio>

/*
    This will not compile. Yet.
*/
int main() {

    // We need to add arrays. Because duh.
    string[] words = new string[3];
    words[0] = "Words";
    words[1] = "are";
    words[2] = "hard.";

    // Functions like a for loop in Python. Iterates over collections.
    // second arg must either be an array or a function that returns one.
    for(string word in words) {
        printf("%s ", word);
    }
    printf("\n");
    return 0;
}