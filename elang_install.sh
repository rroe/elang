echo "Installing prerequisite packages ..."
apt-get install clang -y
apt-get install gcc -y
apt-get install llvm -y
echo "Retrieving elang compiler and jar..."
wget http://lunardb.com/elang/elangc
wget http://lunardb.com/elang/elang.jar
mv elangc /bin
echo "Creating elang bin directory..."
mkdir /bin/elang
mv elang.jar /bin/elang
echo "Setting up file permissions..."
chmod a+x /bin/elang/elang.jar
chmod a+x /bin/elangc
echo "Done!"
