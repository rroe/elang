#import <stdio>

string fizz = "Fizz";
string buzz = "Buzz";
string fizz_buzz = "Fizzbuzz";

// Classic - fizzbuzz!
int main() {
        int count = 0;
        when(count % 15 == 0) {
                puts(fizz_buzz);
        }
        when(count % 5 == 0) {
                puts(buzz);
        }
        when(count % 3 == 0) {
                puts(fizz);
        }
        when(count % 15 != 0 && count % 5 != 0 && count % 3 != 0) {
                return printf("%i\n", i);
        }

        while(count <= 100) {
                count = count + 1;
        }

        return 0;
}