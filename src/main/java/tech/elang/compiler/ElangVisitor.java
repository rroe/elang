// Generated from src/main/java/tech/elang/compiler/Elang.g4 by ANTLR 4.5.1

   package tech.elang.compiler;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ElangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ElangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ElangParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(ElangParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#lang}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLang(ElangParser.LangContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#file_import}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile_import(ElangParser.File_importContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#var_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_def(ElangParser.Var_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#var_mutate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_mutate(ElangParser.Var_mutateContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#const_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst_var(ElangParser.Const_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ElangParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#break_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak_op(ElangParser.Break_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(ElangParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#if_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_op(ElangParser.If_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#while_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_op(ElangParser.While_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#when_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhen_op(ElangParser.When_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#if_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_else(ElangParser.If_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#else_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_op(ElangParser.Else_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#func_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_def(ElangParser.Func_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#func_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_call(ElangParser.Func_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#ret}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(ElangParser.RetContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#typedef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedef(ElangParser.TypedefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#struct_typedef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_typedef(ElangParser.Struct_typedefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#paren_params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_params(ElangParser.Paren_paramsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#paren_args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_args(ElangParser.Paren_argsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#paren_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_expr(ElangParser.Paren_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#struct_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_def(ElangParser.Struct_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(ElangParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#comparison}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison(ElangParser.ComparisonContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(ElangParser.SumContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(ElangParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#struct_paren_arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_paren_arg(ElangParser.Struct_paren_argContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(ElangParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#struct_access}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_access(ElangParser.Struct_accessContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(ElangParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#integer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(ElangParser.IntegerContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#type_double}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_double(ElangParser.Type_doubleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ElangParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(ElangParser.StringContext ctx);
}