// Generated from src/main/java/tech/elang/compiler/Elang.g4 by ANTLR 4.5.1

   package tech.elang.compiler;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ElangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		BlockComment=18, Break=19, LineComment=20, ARITHMETIC_OPERATOR=21, LESS=22, 
		LESS_EQUAL=23, GREATER=24, GREATER_EQUAL=25, EQUAL=26, NOT_EQUAL=27, AND=28, 
		OR=29, PLUS=30, MINUS=31, MULT=32, MOD=33, DIVIDE=34, ASSIGN=35, SEMI=36, 
		ID=37, STRING=38, CHAR=39, INT=40, DOUBLE=41, WS=42;
	public static final int
		RULE_program = 0, RULE_lang = 1, RULE_file_import = 2, RULE_var_def = 3, 
		RULE_var_mutate = 4, RULE_const_var = 5, RULE_statement = 6, RULE_break_op = 7, 
		RULE_type = 8, RULE_if_op = 9, RULE_while_op = 10, RULE_when_op = 11, 
		RULE_if_else = 12, RULE_else_op = 13, RULE_func_def = 14, RULE_func_call = 15, 
		RULE_ret = 16, RULE_typedef = 17, RULE_struct_typedef = 18, RULE_paren_params = 19, 
		RULE_paren_args = 20, RULE_paren_expr = 21, RULE_struct_def = 22, RULE_expr = 23, 
		RULE_comparison = 24, RULE_sum = 25, RULE_term = 26, RULE_struct_paren_arg = 27, 
		RULE_id = 28, RULE_struct_access = 29, RULE_number = 30, RULE_integer = 31, 
		RULE_type_double = 32, RULE_string = 33;
	public static final String[] ruleNames = {
		"program", "lang", "file_import", "var_def", "var_mutate", "const_var", 
		"statement", "break_op", "type", "if_op", "while_op", "when_op", "if_else", 
		"else_op", "func_def", "func_call", "ret", "typedef", "struct_typedef", 
		"paren_params", "paren_args", "paren_expr", "struct_def", "expr", "comparison", 
		"sum", "term", "struct_paren_arg", "id", "struct_access", "number", "integer", 
		"type_double", "string"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'#import'", "'const'", "'{'", "'}'", "'int'", "'double'", "'string'", 
		"'if'", "'while'", "'when'", "'else'", "'return'", "'struct'", "'('", 
		"','", "')'", "'->'", null, "'break'", null, null, "'<'", "'<='", "'>'", 
		"'>='", "'=='", "'!='", "'&&'", "'||'", "'+'", "'-'", "'*'", "'%'", "'/'", 
		"'='", "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "BlockComment", "Break", "LineComment", 
		"ARITHMETIC_OPERATOR", "LESS", "LESS_EQUAL", "GREATER", "GREATER_EQUAL", 
		"EQUAL", "NOT_EQUAL", "AND", "OR", "PLUS", "MINUS", "MULT", "MOD", "DIVIDE", 
		"ASSIGN", "SEMI", "ID", "STRING", "CHAR", "INT", "DOUBLE", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Elang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ElangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public List<LangContext> lang() {
			return getRuleContexts(LangContext.class);
		}
		public LangContext lang(int i) {
			return getRuleContext(LangContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(68);
				lang();
				}
				}
				setState(71); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__12) | (1L << ID))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LangContext extends ParserRuleContext {
		public File_importContext file_import() {
			return getRuleContext(File_importContext.class,0);
		}
		public Var_defContext var_def() {
			return getRuleContext(Var_defContext.class,0);
		}
		public Func_defContext func_def() {
			return getRuleContext(Func_defContext.class,0);
		}
		public Struct_defContext struct_def() {
			return getRuleContext(Struct_defContext.class,0);
		}
		public LangContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lang; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitLang(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LangContext lang() throws RecognitionException {
		LangContext _localctx = new LangContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_lang);
		try {
			setState(77);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(73);
				file_import();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(74);
				var_def();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(75);
				func_def();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(76);
				struct_def();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class File_importContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public File_importContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file_import; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitFile_import(this);
			else return visitor.visitChildren(this);
		}
	}

	public final File_importContext file_import() throws RecognitionException {
		File_importContext _localctx = new File_importContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_file_import);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			match(T__0);
			setState(80);
			match(LESS);
			setState(81);
			id();
			setState(82);
			match(GREATER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_defContext extends ParserRuleContext {
		public TypedefContext typedef() {
			return getRuleContext(TypedefContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(ElangParser.ASSIGN, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(ElangParser.SEMI, 0); }
		public Var_mutateContext var_mutate() {
			return getRuleContext(Var_mutateContext.class,0);
		}
		public Var_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_def; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitVar_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_defContext var_def() throws RecognitionException {
		Var_defContext _localctx = new Var_defContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_var_def);
		try {
			setState(90);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(84);
				typedef();
				setState(85);
				match(ASSIGN);
				setState(86);
				term();
				setState(87);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(89);
				var_mutate();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_mutateContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(ElangParser.ASSIGN, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(ElangParser.SEMI, 0); }
		public Struct_accessContext struct_access() {
			return getRuleContext(Struct_accessContext.class,0);
		}
		public Var_mutateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_mutate; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitVar_mutate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_mutateContext var_mutate() throws RecognitionException {
		Var_mutateContext _localctx = new Var_mutateContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_var_mutate);
		try {
			setState(102);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(92);
				id();
				setState(93);
				match(ASSIGN);
				setState(94);
				term();
				setState(95);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(97);
				struct_access();
				setState(98);
				match(ASSIGN);
				setState(99);
				term();
				setState(100);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Const_varContext extends ParserRuleContext {
		public TypedefContext typedef() {
			return getRuleContext(TypedefContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(ElangParser.ASSIGN, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(ElangParser.SEMI, 0); }
		public Const_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_const_var; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitConst_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Const_varContext const_var() throws RecognitionException {
		Const_varContext _localctx = new Const_varContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_const_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(T__1);
			setState(105);
			typedef();
			setState(106);
			match(ASSIGN);
			setState(107);
			term();
			setState(108);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public If_elseContext if_else() {
			return getRuleContext(If_elseContext.class,0);
		}
		public If_opContext if_op() {
			return getRuleContext(If_opContext.class,0);
		}
		public When_opContext when_op() {
			return getRuleContext(When_opContext.class,0);
		}
		public Func_callContext func_call() {
			return getRuleContext(Func_callContext.class,0);
		}
		public Var_defContext var_def() {
			return getRuleContext(Var_defContext.class,0);
		}
		public While_opContext while_op() {
			return getRuleContext(While_opContext.class,0);
		}
		public Break_opContext break_op() {
			return getRuleContext(Break_opContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public RetContext ret() {
			return getRuleContext(RetContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_statement);
		int _la;
		try {
			setState(130);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(110);
				if_else();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(111);
				if_op();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(112);
				when_op();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(113);
				func_call();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(114);
				var_def();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(115);
				while_op();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(116);
				break_op();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(117);
				match(T__2);
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << Break) | (1L << SEMI) | (1L << ID) | (1L << INT) | (1L << DOUBLE))) != 0)) {
					{
					{
					setState(118);
					statement();
					}
					}
					setState(123);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(124);
				match(T__3);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(125);
				ret();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(126);
				expr();
				setState(127);
				match(SEMI);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(129);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Break_opContext extends ParserRuleContext {
		public Break_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_break_op; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitBreak_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Break_opContext break_op() throws RecognitionException {
		Break_opContext _localctx = new Break_opContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_break_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			match(Break);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type);
		try {
			setState(138);
			switch (_input.LA(1)) {
			case T__4:
				enterOuterAlt(_localctx, 1);
				{
				setState(134);
				match(T__4);
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(135);
				match(T__5);
				}
				break;
			case T__6:
				enterOuterAlt(_localctx, 3);
				{
				setState(136);
				match(T__6);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 4);
				{
				setState(137);
				id();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_opContext extends ParserRuleContext {
		public Paren_exprContext paren_expr() {
			return getRuleContext(Paren_exprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public If_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_op; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitIf_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_opContext if_op() throws RecognitionException {
		If_opContext _localctx = new If_opContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_if_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(T__7);
			setState(141);
			paren_expr();
			setState(142);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_opContext extends ParserRuleContext {
		public Paren_exprContext paren_expr() {
			return getRuleContext(Paren_exprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public While_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_op; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitWhile_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_opContext while_op() throws RecognitionException {
		While_opContext _localctx = new While_opContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_while_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			match(T__8);
			setState(145);
			paren_expr();
			setState(146);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class When_opContext extends ParserRuleContext {
		public Paren_exprContext paren_expr() {
			return getRuleContext(Paren_exprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public When_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_when_op; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitWhen_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final When_opContext when_op() throws RecognitionException {
		When_opContext _localctx = new When_opContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_when_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(148);
			match(T__9);
			setState(149);
			paren_expr();
			setState(150);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_elseContext extends ParserRuleContext {
		public Paren_exprContext paren_expr() {
			return getRuleContext(Paren_exprContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Else_opContext else_op() {
			return getRuleContext(Else_opContext.class,0);
		}
		public If_elseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_else; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitIf_else(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_elseContext if_else() throws RecognitionException {
		If_elseContext _localctx = new If_elseContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_if_else);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			match(T__7);
			setState(153);
			paren_expr();
			setState(154);
			statement();
			setState(155);
			else_op();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_opContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Else_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_op; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitElse_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_opContext else_op() throws RecognitionException {
		Else_opContext _localctx = new Else_opContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_else_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__10);
			setState(158);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_defContext extends ParserRuleContext {
		public TypedefContext typedef() {
			return getRuleContext(TypedefContext.class,0);
		}
		public Paren_paramsContext paren_params() {
			return getRuleContext(Paren_paramsContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Func_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_def; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitFunc_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_defContext func_def() throws RecognitionException {
		Func_defContext _localctx = new Func_defContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_func_def);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			typedef();
			setState(161);
			paren_params();
			setState(163); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(162);
					statement();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(165); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_callContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Paren_argsContext paren_args() {
			return getRuleContext(Paren_argsContext.class,0);
		}
		public Func_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_call; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitFunc_call(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_callContext func_call() throws RecognitionException {
		Func_callContext _localctx = new Func_callContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_func_call);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(167);
			id();
			setState(168);
			paren_args();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RetContext extends ParserRuleContext {
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public RetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ret; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitRet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RetContext ret() throws RecognitionException {
		RetContext _localctx = new RetContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_ret);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			match(T__11);
			setState(171);
			term();
			setState(172);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedefContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Struct_typedefContext struct_typedef() {
			return getRuleContext(Struct_typedefContext.class,0);
		}
		public TypedefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedef; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitTypedef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypedefContext typedef() throws RecognitionException {
		TypedefContext _localctx = new TypedefContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_typedef);
		try {
			setState(178);
			switch (_input.LA(1)) {
			case T__4:
			case T__5:
			case T__6:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(174);
				type();
				setState(175);
				id();
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 2);
				{
				setState(177);
				struct_typedef();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_typedefContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Struct_typedefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_typedef; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitStruct_typedef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_typedefContext struct_typedef() throws RecognitionException {
		Struct_typedefContext _localctx = new Struct_typedefContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_struct_typedef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			match(T__12);
			setState(181);
			type();
			setState(182);
			id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Paren_paramsContext extends ParserRuleContext {
		public List<TypedefContext> typedef() {
			return getRuleContexts(TypedefContext.class);
		}
		public TypedefContext typedef(int i) {
			return getRuleContext(TypedefContext.class,i);
		}
		public Paren_paramsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paren_params; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitParen_params(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Paren_paramsContext paren_params() throws RecognitionException {
		Paren_paramsContext _localctx = new Paren_paramsContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_paren_params);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			match(T__13);
			setState(190);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__12) | (1L << T__14) | (1L << ID))) != 0)) {
				{
				setState(188);
				switch (_input.LA(1)) {
				case T__4:
				case T__5:
				case T__6:
				case T__12:
				case ID:
					{
					setState(185);
					typedef();
					}
					break;
				case T__14:
					{
					{
					setState(186);
					match(T__14);
					setState(187);
					typedef();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(192);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(193);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Paren_argsContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public Paren_argsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paren_args; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitParen_args(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Paren_argsContext paren_args() throws RecognitionException {
		Paren_argsContext _localctx = new Paren_argsContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_paren_args);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(T__13);
			setState(201);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << ID) | (1L << STRING) | (1L << INT) | (1L << DOUBLE))) != 0)) {
				{
				setState(199);
				switch (_input.LA(1)) {
				case T__12:
				case T__13:
				case ID:
				case STRING:
				case INT:
				case DOUBLE:
					{
					setState(196);
					term();
					}
					break;
				case T__14:
					{
					{
					setState(197);
					match(T__14);
					setState(198);
					term();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(203);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(204);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Paren_exprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Paren_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paren_expr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitParen_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Paren_exprContext paren_expr() throws RecognitionException {
		Paren_exprContext _localctx = new Paren_exprContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_paren_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			match(T__13);
			setState(207);
			expr();
			setState(208);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_defContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TypedefContext> typedef() {
			return getRuleContexts(TypedefContext.class);
		}
		public TypedefContext typedef(int i) {
			return getRuleContext(TypedefContext.class,i);
		}
		public Struct_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_def; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitStruct_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_defContext struct_def() throws RecognitionException {
		Struct_defContext _localctx = new Struct_defContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_struct_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(T__12);
			setState(211);
			id();
			setState(212);
			match(T__2);
			setState(216); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(213);
				typedef();
				setState(214);
				match(SEMI);
				}
				}
				setState(218); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__12) | (1L << ID))) != 0) );
			setState(220);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ComparisonContext comparison() {
			return getRuleContext(ComparisonContext.class,0);
		}
		public SumContext sum() {
			return getRuleContext(SumContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_expr);
		try {
			setState(224);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(222);
				comparison(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(223);
				sum(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public List<SumContext> sum() {
			return getRuleContexts(SumContext.class);
		}
		public SumContext sum(int i) {
			return getRuleContext(SumContext.class,i);
		}
		public TerminalNode LESS() { return getToken(ElangParser.LESS, 0); }
		public TerminalNode GREATER() { return getToken(ElangParser.GREATER, 0); }
		public TerminalNode LESS_EQUAL() { return getToken(ElangParser.LESS_EQUAL, 0); }
		public TerminalNode GREATER_EQUAL() { return getToken(ElangParser.GREATER_EQUAL, 0); }
		public TerminalNode EQUAL() { return getToken(ElangParser.EQUAL, 0); }
		public TerminalNode NOT_EQUAL() { return getToken(ElangParser.NOT_EQUAL, 0); }
		public List<ComparisonContext> comparison() {
			return getRuleContexts(ComparisonContext.class);
		}
		public ComparisonContext comparison(int i) {
			return getRuleContext(ComparisonContext.class,i);
		}
		public TerminalNode AND() { return getToken(ElangParser.AND, 0); }
		public TerminalNode OR() { return getToken(ElangParser.OR, 0); }
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitComparison(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		return comparison(0);
	}

	private ComparisonContext comparison(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ComparisonContext _localctx = new ComparisonContext(_ctx, _parentState);
		ComparisonContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_comparison, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(251);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(227);
				sum(0);
				setState(228);
				match(LESS);
				setState(229);
				sum(0);
				}
				break;
			case 2:
				{
				setState(231);
				sum(0);
				setState(232);
				match(GREATER);
				setState(233);
				sum(0);
				}
				break;
			case 3:
				{
				setState(235);
				sum(0);
				setState(236);
				match(LESS_EQUAL);
				setState(237);
				sum(0);
				}
				break;
			case 4:
				{
				setState(239);
				sum(0);
				setState(240);
				match(GREATER_EQUAL);
				setState(241);
				sum(0);
				}
				break;
			case 5:
				{
				setState(243);
				sum(0);
				setState(244);
				match(EQUAL);
				setState(245);
				sum(0);
				}
				break;
			case 6:
				{
				setState(247);
				sum(0);
				setState(248);
				match(NOT_EQUAL);
				setState(249);
				sum(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(261);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(259);
					switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
					case 1:
						{
						_localctx = new ComparisonContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_comparison);
						setState(253);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(254);
						match(AND);
						setState(255);
						comparison(3);
						}
						break;
					case 2:
						{
						_localctx = new ComparisonContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_comparison);
						setState(256);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(257);
						match(OR);
						setState(258);
						comparison(2);
						}
						break;
					}
					} 
				}
				setState(263);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SumContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public Func_callContext func_call() {
			return getRuleContext(Func_callContext.class,0);
		}
		public Paren_exprContext paren_expr() {
			return getRuleContext(Paren_exprContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Struct_accessContext struct_access() {
			return getRuleContext(Struct_accessContext.class,0);
		}
		public List<SumContext> sum() {
			return getRuleContexts(SumContext.class);
		}
		public SumContext sum(int i) {
			return getRuleContext(SumContext.class,i);
		}
		public TerminalNode ARITHMETIC_OPERATOR() { return getToken(ElangParser.ARITHMETIC_OPERATOR, 0); }
		public SumContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sum; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitSum(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SumContext sum() throws RecognitionException {
		return sum(0);
	}

	private SumContext sum(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SumContext _localctx = new SumContext(_ctx, _parentState);
		SumContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_sum, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(265);
				number();
				}
				break;
			case 2:
				{
				setState(266);
				func_call();
				}
				break;
			case 3:
				{
				setState(267);
				paren_expr();
				}
				break;
			case 4:
				{
				setState(268);
				id();
				}
				break;
			case 5:
				{
				setState(269);
				struct_access();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(277);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new SumContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_sum);
					setState(272);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(273);
					match(ARITHMETIC_OPERATOR);
					setState(274);
					sum(2);
					}
					} 
				}
				setState(279);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public Struct_paren_argContext struct_paren_arg() {
			return getRuleContext(Struct_paren_argContext.class,0);
		}
		public SumContext sum() {
			return getRuleContext(SumContext.class,0);
		}
		public ComparisonContext comparison() {
			return getRuleContext(ComparisonContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_term);
		try {
			setState(284);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(280);
				string();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(281);
				struct_paren_arg();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(282);
				sum(0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(283);
				comparison(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_paren_argContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Struct_paren_argContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_paren_arg; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitStruct_paren_arg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_paren_argContext struct_paren_arg() throws RecognitionException {
		Struct_paren_argContext _localctx = new Struct_paren_argContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_struct_paren_arg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			match(T__12);
			setState(287);
			id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(ElangParser.ID, 0); }
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_accessContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public Struct_accessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_access; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitStruct_access(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_accessContext struct_access() throws RecognitionException {
		Struct_accessContext _localctx = new Struct_accessContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_struct_access);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			id();
			setState(292);
			match(T__16);
			setState(293);
			id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public Type_doubleContext type_double() {
			return getRuleContext(Type_doubleContext.class,0);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_number);
		try {
			setState(297);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(295);
				integer();
				}
				break;
			case DOUBLE:
				enterOuterAlt(_localctx, 2);
				{
				setState(296);
				type_double();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(ElangParser.INT, 0); }
		public IntegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitInteger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegerContext integer() throws RecognitionException {
		IntegerContext _localctx = new IntegerContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_integer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(299);
			match(INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_doubleContext extends ParserRuleContext {
		public TerminalNode DOUBLE() { return getToken(ElangParser.DOUBLE, 0); }
		public Type_doubleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_double; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitType_double(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_doubleContext type_double() throws RecognitionException {
		Type_doubleContext _localctx = new Type_doubleContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_type_double);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(301);
			match(DOUBLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(ElangParser.STRING, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ElangVisitor ) return ((ElangVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(303);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 24:
			return comparison_sempred((ComparisonContext)_localctx, predIndex);
		case 25:
			return sum_sempred((SumContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean comparison_sempred(ComparisonContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean sum_sempred(SumContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3,\u0134\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\3\2\6\2H\n\2\r\2\16\2I\3\3\3\3\3\3\3\3\5\3P\n\3\3\4"+
		"\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5]\n\5\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\5\6i\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\7\bz\n\b\f\b\16\b}\13\b\3\b\3\b\3\b\3\b\3\b\3\b\5"+
		"\b\u0085\n\b\3\t\3\t\3\n\3\n\3\n\3\n\5\n\u008d\n\n\3\13\3\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3"+
		"\17\3\20\3\20\3\20\6\20\u00a6\n\20\r\20\16\20\u00a7\3\21\3\21\3\21\3\22"+
		"\3\22\3\22\3\22\3\23\3\23\3\23\3\23\5\23\u00b5\n\23\3\24\3\24\3\24\3\24"+
		"\3\25\3\25\3\25\3\25\7\25\u00bf\n\25\f\25\16\25\u00c2\13\25\3\25\3\25"+
		"\3\26\3\26\3\26\3\26\7\26\u00ca\n\26\f\26\16\26\u00cd\13\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\6\30\u00db\n\30\r\30"+
		"\16\30\u00dc\3\30\3\30\3\31\3\31\5\31\u00e3\n\31\3\32\3\32\3\32\3\32\3"+
		"\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3"+
		"\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u00fe\n\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\7\32\u0106\n\32\f\32\16\32\u0109\13\32\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\5\33\u0111\n\33\3\33\3\33\3\33\7\33\u0116\n\33\f\33\16\33\u0119"+
		"\13\33\3\34\3\34\3\34\3\34\5\34\u011f\n\34\3\35\3\35\3\35\3\36\3\36\3"+
		"\37\3\37\3\37\3\37\3 \3 \5 \u012c\n \3!\3!\3\"\3\"\3#\3#\3#\2\4\62\64"+
		"$\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BD\2"+
		"\2\u013d\2G\3\2\2\2\4O\3\2\2\2\6Q\3\2\2\2\b\\\3\2\2\2\nh\3\2\2\2\fj\3"+
		"\2\2\2\16\u0084\3\2\2\2\20\u0086\3\2\2\2\22\u008c\3\2\2\2\24\u008e\3\2"+
		"\2\2\26\u0092\3\2\2\2\30\u0096\3\2\2\2\32\u009a\3\2\2\2\34\u009f\3\2\2"+
		"\2\36\u00a2\3\2\2\2 \u00a9\3\2\2\2\"\u00ac\3\2\2\2$\u00b4\3\2\2\2&\u00b6"+
		"\3\2\2\2(\u00ba\3\2\2\2*\u00c5\3\2\2\2,\u00d0\3\2\2\2.\u00d4\3\2\2\2\60"+
		"\u00e2\3\2\2\2\62\u00fd\3\2\2\2\64\u0110\3\2\2\2\66\u011e\3\2\2\28\u0120"+
		"\3\2\2\2:\u0123\3\2\2\2<\u0125\3\2\2\2>\u012b\3\2\2\2@\u012d\3\2\2\2B"+
		"\u012f\3\2\2\2D\u0131\3\2\2\2FH\5\4\3\2GF\3\2\2\2HI\3\2\2\2IG\3\2\2\2"+
		"IJ\3\2\2\2J\3\3\2\2\2KP\5\6\4\2LP\5\b\5\2MP\5\36\20\2NP\5.\30\2OK\3\2"+
		"\2\2OL\3\2\2\2OM\3\2\2\2ON\3\2\2\2P\5\3\2\2\2QR\7\3\2\2RS\7\30\2\2ST\5"+
		":\36\2TU\7\32\2\2U\7\3\2\2\2VW\5$\23\2WX\7%\2\2XY\5\66\34\2YZ\7&\2\2Z"+
		"]\3\2\2\2[]\5\n\6\2\\V\3\2\2\2\\[\3\2\2\2]\t\3\2\2\2^_\5:\36\2_`\7%\2"+
		"\2`a\5\66\34\2ab\7&\2\2bi\3\2\2\2cd\5<\37\2de\7%\2\2ef\5\66\34\2fg\7&"+
		"\2\2gi\3\2\2\2h^\3\2\2\2hc\3\2\2\2i\13\3\2\2\2jk\7\4\2\2kl\5$\23\2lm\7"+
		"%\2\2mn\5\66\34\2no\7&\2\2o\r\3\2\2\2p\u0085\5\32\16\2q\u0085\5\24\13"+
		"\2r\u0085\5\30\r\2s\u0085\5 \21\2t\u0085\5\b\5\2u\u0085\5\26\f\2v\u0085"+
		"\5\20\t\2w{\7\5\2\2xz\5\16\b\2yx\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3\2\2\2"+
		"|~\3\2\2\2}{\3\2\2\2~\u0085\7\6\2\2\177\u0085\5\"\22\2\u0080\u0081\5\60"+
		"\31\2\u0081\u0082\7&\2\2\u0082\u0085\3\2\2\2\u0083\u0085\7&\2\2\u0084"+
		"p\3\2\2\2\u0084q\3\2\2\2\u0084r\3\2\2\2\u0084s\3\2\2\2\u0084t\3\2\2\2"+
		"\u0084u\3\2\2\2\u0084v\3\2\2\2\u0084w\3\2\2\2\u0084\177\3\2\2\2\u0084"+
		"\u0080\3\2\2\2\u0084\u0083\3\2\2\2\u0085\17\3\2\2\2\u0086\u0087\7\25\2"+
		"\2\u0087\21\3\2\2\2\u0088\u008d\7\7\2\2\u0089\u008d\7\b\2\2\u008a\u008d"+
		"\7\t\2\2\u008b\u008d\5:\36\2\u008c\u0088\3\2\2\2\u008c\u0089\3\2\2\2\u008c"+
		"\u008a\3\2\2\2\u008c\u008b\3\2\2\2\u008d\23\3\2\2\2\u008e\u008f\7\n\2"+
		"\2\u008f\u0090\5,\27\2\u0090\u0091\5\16\b\2\u0091\25\3\2\2\2\u0092\u0093"+
		"\7\13\2\2\u0093\u0094\5,\27\2\u0094\u0095\5\16\b\2\u0095\27\3\2\2\2\u0096"+
		"\u0097\7\f\2\2\u0097\u0098\5,\27\2\u0098\u0099\5\16\b\2\u0099\31\3\2\2"+
		"\2\u009a\u009b\7\n\2\2\u009b\u009c\5,\27\2\u009c\u009d\5\16\b\2\u009d"+
		"\u009e\5\34\17\2\u009e\33\3\2\2\2\u009f\u00a0\7\r\2\2\u00a0\u00a1\5\16"+
		"\b\2\u00a1\35\3\2\2\2\u00a2\u00a3\5$\23\2\u00a3\u00a5\5(\25\2\u00a4\u00a6"+
		"\5\16\b\2\u00a5\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a5\3\2\2\2"+
		"\u00a7\u00a8\3\2\2\2\u00a8\37\3\2\2\2\u00a9\u00aa\5:\36\2\u00aa\u00ab"+
		"\5*\26\2\u00ab!\3\2\2\2\u00ac\u00ad\7\16\2\2\u00ad\u00ae\5\66\34\2\u00ae"+
		"\u00af\7&\2\2\u00af#\3\2\2\2\u00b0\u00b1\5\22\n\2\u00b1\u00b2\5:\36\2"+
		"\u00b2\u00b5\3\2\2\2\u00b3\u00b5\5&\24\2\u00b4\u00b0\3\2\2\2\u00b4\u00b3"+
		"\3\2\2\2\u00b5%\3\2\2\2\u00b6\u00b7\7\17\2\2\u00b7\u00b8\5\22\n\2\u00b8"+
		"\u00b9\5:\36\2\u00b9\'\3\2\2\2\u00ba\u00c0\7\20\2\2\u00bb\u00bf\5$\23"+
		"\2\u00bc\u00bd\7\21\2\2\u00bd\u00bf\5$\23\2\u00be\u00bb\3\2\2\2\u00be"+
		"\u00bc\3\2\2\2\u00bf\u00c2\3\2\2\2\u00c0\u00be\3\2\2\2\u00c0\u00c1\3\2"+
		"\2\2\u00c1\u00c3\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c3\u00c4\7\22\2\2\u00c4"+
		")\3\2\2\2\u00c5\u00cb\7\20\2\2\u00c6\u00ca\5\66\34\2\u00c7\u00c8\7\21"+
		"\2\2\u00c8\u00ca\5\66\34\2\u00c9\u00c6\3\2\2\2\u00c9\u00c7\3\2\2\2\u00ca"+
		"\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00ce\3\2"+
		"\2\2\u00cd\u00cb\3\2\2\2\u00ce\u00cf\7\22\2\2\u00cf+\3\2\2\2\u00d0\u00d1"+
		"\7\20\2\2\u00d1\u00d2\5\60\31\2\u00d2\u00d3\7\22\2\2\u00d3-\3\2\2\2\u00d4"+
		"\u00d5\7\17\2\2\u00d5\u00d6\5:\36\2\u00d6\u00da\7\5\2\2\u00d7\u00d8\5"+
		"$\23\2\u00d8\u00d9\7&\2\2\u00d9\u00db\3\2\2\2\u00da\u00d7\3\2\2\2\u00db"+
		"\u00dc\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de\3\2"+
		"\2\2\u00de\u00df\7\6\2\2\u00df/\3\2\2\2\u00e0\u00e3\5\62\32\2\u00e1\u00e3"+
		"\5\64\33\2\u00e2\u00e0\3\2\2\2\u00e2\u00e1\3\2\2\2\u00e3\61\3\2\2\2\u00e4"+
		"\u00e5\b\32\1\2\u00e5\u00e6\5\64\33\2\u00e6\u00e7\7\30\2\2\u00e7\u00e8"+
		"\5\64\33\2\u00e8\u00fe\3\2\2\2\u00e9\u00ea\5\64\33\2\u00ea\u00eb\7\32"+
		"\2\2\u00eb\u00ec\5\64\33\2\u00ec\u00fe\3\2\2\2\u00ed\u00ee\5\64\33\2\u00ee"+
		"\u00ef\7\31\2\2\u00ef\u00f0\5\64\33\2\u00f0\u00fe\3\2\2\2\u00f1\u00f2"+
		"\5\64\33\2\u00f2\u00f3\7\33\2\2\u00f3\u00f4\5\64\33\2\u00f4\u00fe\3\2"+
		"\2\2\u00f5\u00f6\5\64\33\2\u00f6\u00f7\7\34\2\2\u00f7\u00f8\5\64\33\2"+
		"\u00f8\u00fe\3\2\2\2\u00f9\u00fa\5\64\33\2\u00fa\u00fb\7\35\2\2\u00fb"+
		"\u00fc\5\64\33\2\u00fc\u00fe\3\2\2\2\u00fd\u00e4\3\2\2\2\u00fd\u00e9\3"+
		"\2\2\2\u00fd\u00ed\3\2\2\2\u00fd\u00f1\3\2\2\2\u00fd\u00f5\3\2\2\2\u00fd"+
		"\u00f9\3\2\2\2\u00fe\u0107\3\2\2\2\u00ff\u0100\f\4\2\2\u0100\u0101\7\36"+
		"\2\2\u0101\u0106\5\62\32\5\u0102\u0103\f\3\2\2\u0103\u0104\7\37\2\2\u0104"+
		"\u0106\5\62\32\4\u0105\u00ff\3\2\2\2\u0105\u0102\3\2\2\2\u0106\u0109\3"+
		"\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2\u0108\63\3\2\2\2\u0109"+
		"\u0107\3\2\2\2\u010a\u010b\b\33\1\2\u010b\u0111\5> \2\u010c\u0111\5 \21"+
		"\2\u010d\u0111\5,\27\2\u010e\u0111\5:\36\2\u010f\u0111\5<\37\2\u0110\u010a"+
		"\3\2\2\2\u0110\u010c\3\2\2\2\u0110\u010d\3\2\2\2\u0110\u010e\3\2\2\2\u0110"+
		"\u010f\3\2\2\2\u0111\u0117\3\2\2\2\u0112\u0113\f\3\2\2\u0113\u0114\7\27"+
		"\2\2\u0114\u0116\5\64\33\4\u0115\u0112\3\2\2\2\u0116\u0119\3\2\2\2\u0117"+
		"\u0115\3\2\2\2\u0117\u0118\3\2\2\2\u0118\65\3\2\2\2\u0119\u0117\3\2\2"+
		"\2\u011a\u011f\5D#\2\u011b\u011f\58\35\2\u011c\u011f\5\64\33\2\u011d\u011f"+
		"\5\62\32\2\u011e\u011a\3\2\2\2\u011e\u011b\3\2\2\2\u011e\u011c\3\2\2\2"+
		"\u011e\u011d\3\2\2\2\u011f\67\3\2\2\2\u0120\u0121\7\17\2\2\u0121\u0122"+
		"\5:\36\2\u01229\3\2\2\2\u0123\u0124\7\'\2\2\u0124;\3\2\2\2\u0125\u0126"+
		"\5:\36\2\u0126\u0127\7\23\2\2\u0127\u0128\5:\36\2\u0128=\3\2\2\2\u0129"+
		"\u012c\5@!\2\u012a\u012c\5B\"\2\u012b\u0129\3\2\2\2\u012b\u012a\3\2\2"+
		"\2\u012c?\3\2\2\2\u012d\u012e\7*\2\2\u012eA\3\2\2\2\u012f\u0130\7+\2\2"+
		"\u0130C\3\2\2\2\u0131\u0132\7(\2\2\u0132E\3\2\2\2\30IO\\h{\u0084\u008c"+
		"\u00a7\u00b4\u00be\u00c0\u00c9\u00cb\u00dc\u00e2\u00fd\u0105\u0107\u0110"+
		"\u0117\u011e\u012b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}