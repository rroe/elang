package tech.elang.compiler;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ricky on 10/15/2016.
 */
public class ElangCompiler extends ElangBaseVisitor<String> implements ElangVisitor<String> {

    private List<ElangParser.When_opContext> whens = new ArrayList<>();

    private String buildFrom(ArrayList<String> in) {
        String out = "";
        for (String s : in) {
            out = out + s + "\n";
        }
        return out;
    }

    private void debug(String s) {
        System.out.println("[DEBUG] =======\n" + s + "\n[/DEBUG] ========");
    }

    @Override
    public String visitProgram(ElangParser.ProgramContext ctx) {
        ArrayList<String> out = new ArrayList<String>();
        for (ElangParser.LangContext lang : ctx.lang()) {
            if(lang.func_def() != null) {
                out.add(this.visitFunc_def(lang.func_def()));
            } else if(lang.var_def() != null) {
                out.add(this.visitVar_def(lang.var_def()));
            } else if(lang.file_import() != null) {
                String temp = "#include <" + lang.file_import().id().getText() + ".h>";
                out.add(temp);
            } else if(lang.struct_def() != null) {
                out.add(this.visitStruct_def(lang.struct_def()));
            }
        }

        return this.buildFrom(out);
    }

    @Override
    public String visitStruct_def(ElangParser.Struct_defContext ctx) {
        String out = "";
        out = out + "struct " + this.visitId(ctx.id()) + " {\n";
        for(ElangParser.TypedefContext typedef : ctx.typedef()) {
            out = out + this.visitTypedef(typedef) + ";\n";
        }
        out = out + "};\n";
        return out;
    }

    @Override
    public String visitLang(ElangParser.LangContext ctx) {
        // This is being done in the prog visitor
        // @see: visitProgram
        return null;
    }

    @Override
    public String visitVar_def(ElangParser.Var_defContext ctx) {
        String const_word = "";
        if(ctx.var_mutate() != null) {
            String word = "";
            if(ctx.var_mutate().struct_access() != null) {
                word = ctx.var_mutate().struct_access().getText();
            } else {
                word = ctx.var_mutate().id().getText();
            }
            String id = word;
            String assign = " = ";
            String term = ctx.var_mutate().term().getText();
            String checkWhens = "";
            ParserRuleContext parent = ctx.getParent();
            boolean draw = true;
            while(parent != null) {
                if(parent.getRuleContext().getText().startsWith("when")) {
                    draw = false;
                }
                parent = parent.getParent();
            }
            for(ElangParser.When_opContext when : this.whens) {
                if(draw)
                    checkWhens = checkWhens + this.translateWhen(when);
            }
            return id + assign + term + ctx.var_mutate().SEMI() + "\n" + checkWhens + "\n";

        } else {
            String typedef = this.visitTypedef(ctx.typedef());
            String assign = " = ";
//        String term = this.visitTerm(ctx.term());
            String term = this.visitTerm(ctx.term());
//        System.out.println(ctx.term().getText());
            return const_word + typedef + assign + term + ctx.SEMI() + "\n";
        }
    }

    @Override
    public String visitStatement(ElangParser.StatementContext ctx) {
        String out = "";
        if(ctx.statement().size() == 0) {
            if (ctx.func_call() != null) {
                out = out + this.visitFunc_call(ctx.func_call()) + ";\n";
            }
            if (ctx.ret() != null) {
                out = out + this.visitRet(ctx.ret());
            }
            if (ctx.var_def() != null) {
                out = out + this.visitVar_def(ctx.var_def());
            }
            if (ctx.if_op() != null) {
                out = out + this.visitIf_op(ctx.if_op());
            }
            if (ctx.if_else() != null) {
                out = out + this.visitIf_else(ctx.if_else());
            }
            if (ctx.while_op() != null) {
                out = out + this.visitWhile_op(ctx.while_op());
            }
            if (ctx.when_op() != null) {
                out = out + this.visitWhen_op(ctx.when_op());
            }
            if (ctx.break_op() != null) {
                out = out + this.visitBreak_op(ctx.break_op());
            }
        } else {
            for (ElangParser.StatementContext stmt : ctx.statement()) {
                out = out + this.visitStatement(stmt);
            }
        }

        return out;
    }

    @Override
    public String visitBreak_op(ElangParser.Break_opContext ctx) {
        return "break;\n";
    }

    @Override
    public String visitType(ElangParser.TypeContext ctx) {
        if(ctx.getText().equals("string")) {
            return "char*";
        }
        return ctx.getText();
    }

    @Override
    public String visitWhile_op(ElangParser.While_opContext ctx) {
        String out = "while ";
        out = out + this.visitParen_expr(ctx.paren_expr()) + " {\n";
        out = out + this.visitStatement(ctx.statement());
        out = out + "}\n";
        return out;
    }

    @Override
    public String visitWhen_op(ElangParser.When_opContext ctx) {
        this.whens.add(ctx);
        return "";
    }

    public String translateWhen(ElangParser.When_opContext ctx) {
        String out = "";
        out = out + "if " + this.visitParen_expr(ctx.paren_expr()) + " {\n";
        out = out + this.visitStatement(ctx.statement()) + "}\n";
        return out;
    }

    @Override
    public String visitIf_op(ElangParser.If_opContext ctx) {
        String out = "if ";
        out = out + this.visitParen_expr(ctx.paren_expr());
        out = out + "{\n";
        out = out + this.visitStatement(ctx.statement());
        out = out + "}\n";
        return out;
    }

    @Override
    public String visitIf_else(ElangParser.If_elseContext ctx) {
        String out = "if ";
        out = out + this.visitParen_expr(ctx.paren_expr());
        out = out + "{\n";
        out = out + this.visitStatement(ctx.statement());
        out = out + "}\n";
        out = out + " else { \n";
        out = out + this.visitElse_op(ctx.else_op());
        out = out + "}\n";
        return out;
    }

    @Override
    public String visitElse_op(ElangParser.Else_opContext ctx) {
        String out = "";
        out = out + this.visitStatement(ctx.statement());
        return out;
    }

    @Override
    public String visitFunc_def(ElangParser.Func_defContext ctx) {
        String typedef = this.visitTypedef(ctx.typedef());
        String out = typedef + " " + this.visitParen_params(ctx.paren_params());
        out = out + " { \n";
        for (ElangParser.StatementContext stmt : ctx.statement()) {
            out = out + this.visitStatement(stmt);
        }
        out = out + " }\n";
        return out;
    }

    @Override
    public String visitFunc_call(ElangParser.Func_callContext ctx) {
        String out = "";
//        System.out.println("=" + ctx.paren_args().getText());
        out = out + ctx.id().getText() + this.visitParen_args(ctx.paren_args());
        return out;
    }

    @Override
    public String visitRet(ElangParser.RetContext ctx) {
        return "return " + this.visitTerm(ctx.term()) + ";\n";
    }

    @Override
    public String visitTypedef(ElangParser.TypedefContext ctx) {
        try {
            if(ctx.struct_typedef() != null) {
                ElangParser.Struct_typedefContext str = ctx.struct_typedef();
                String out = "struct " + this.visitType(str.type()) + " " + this.visitId(str.id());
                return out;
            }
            String type = this.visitType(ctx.type());
            String name = this.visitId(ctx.id());
            return type + " " + name;
        }catch(NullPointerException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String visitParen_params(ElangParser.Paren_paramsContext ctx) {
        String out = "(";
        int i = 0;
        for (ElangParser.TypedefContext type : ctx.typedef()) {
            out = out + this.visitTypedef(type);
            if (i < ctx.typedef().size() - 1) {
                i = i + 1;
                out = out + ", ";
            }
        }
        out = out + ")";
        return out;
    }

    @Override
    public String visitParen_args(ElangParser.Paren_argsContext ctx) {
        String out = "(";
        int i = 0;
        for (ElangParser.TermContext term :  ctx.term()) {
//            if(term.struct_paren_arg() != null) {
//
//            }
            out = out + this.visitTerm(term);
            if (i < ctx.term().size() - 1) {
                i = i + 1;
                out = out + ", ";
            }
        }
        out = out + ")";
        return out;
    }

    @Override
    public String visitParen_expr(ElangParser.Paren_exprContext ctx) {
        String out = "(" + this.visitExpr(ctx.expr()) + ")";
        return out;
    }

    @Override
    public String visitExpr(ElangParser.ExprContext ctx) {
        String out = "";

        if (ctx.comparison() != null) {
            out = out + this.visitComparison(ctx.comparison());
        }
        if (ctx.sum() != null) {
            out = out + this.visitSum(ctx.sum());
        }

        return out;
    }

    @Override
    public String visitComparison(ElangParser.ComparisonContext ctx) {
//        debug(ctx.getText());
        // TODO: Super hacky right now, but I don't really care.
//        String out = "";
//        if (ctx.sum() != null) {
//            for (ElangParser.SumContext sum : ctx.sum()) {
//                out = out + this.visitSum(sum);
//            }
//        } else {
//            out = out + ctx.getText();
//        }
        return ctx.getText();
    }

    @Override
    public String visitSum(ElangParser.SumContext ctx) {
//        if(ctx.typedef() != null) {
//            return this.visitTypedef(ctx.typedef());
//        }
        if(ctx.func_call() != null) {
            return this.visitFunc_call(ctx.func_call());
        }
        String out = ctx.getText();
        return out;
    }

    @Override
    public String visitTerm(ElangParser.TermContext ctx) {
        if(ctx.struct_paren_arg() != null) {
            return "struct " + this.visitId(ctx.struct_paren_arg().id());
        }
        if (ctx.sum() != null) {
            return this.visitSum(ctx.sum());
        }
//        if(ctx.typedef() != null) {
//            return this.visitTypedef(ctx.typedef());
//        }
        return ctx.getText();
//
//
//        if (ctx.id() != null) {
//            return this.visitId(ctx.id());
//        }
//
//        if (ctx.integer() != null) {
//            return this.visitInteger(ctx.integer());
//        }
//
//        if (ctx.comparison() != null) {
//            return ctx.comparison().getText();
//        }
//
//        if (ctx.string() != null) {
//            return ctx.string().getText();
//        }
//
//        if (ctx.sum() != null) {
//            return this.visitSum(ctx.sum());
//        }
//
//        if (ctx.paren_expr() != null) {
//            return this.visitParen_expr(ctx.paren_expr());
//        }
//
//        return null;
    }

    @Override
    public String visitId(ElangParser.IdContext ctx) {
        return ctx.getText();
    }

    @Override
    public String visitNumber(ElangParser.NumberContext ctx) {
        return null;
    }

    @Override
    public String visitInteger(ElangParser.IntegerContext ctx) {
        return ctx.getText();
    }

    @Override
    public String visitType_double(ElangParser.Type_doubleContext ctx) {
        return null;
    }

    @Override
    public String visitString(ElangParser.StringContext ctx) {
        return null;
    }

    @Override
    public String visit(ParseTree parseTree) {
        return null;
    }

    @Override
    public String visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public String visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public String visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
