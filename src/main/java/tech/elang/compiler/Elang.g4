grammar Elang;

@header {
   package tech.elang.compiler;
}

/*
 *  <program> ::= <statement>
 *  <statement> ::= "if" <paren_expr> <statement> |
 *                  "if" <paren_expr> <statement> "else" <statement> |
 *                  "while" <paren_expr> <statement> |
 *                  "do" <statement> "while" <paren_expr> ";" |
 *                  "{" { <statement> } "}" |
 *                  <expr> ";" |
 *                  ";"
 *  <paren_expr> ::= "(" <expr> ")"
 *  <expr> ::= <test> | <id> "=" <expr>
 *  <test> ::= <sum> | <sum> "<" <sum>
 *  <sum> ::= <term> | <sum> "+" <term> | <sum> "-" <term>
 *  <term> ::= <id> | <int> | <paren_expr>
 *  <id> ::= "a" | "b" | "c" | "d" | ... | "z"
 *  <int> ::= <an_unsigned_decimal_integer>
*/

program
    : lang+
    ;

lang
   : file_import
   | var_def
   | func_def
   | struct_def
   ;

file_import
    : '#import' '<' id '>'
    ;

var_def
    : typedef ASSIGN term SEMI
    | var_mutate
    ;

var_mutate
    : id ASSIGN term SEMI
    | struct_access ASSIGN term SEMI
    ;

const_var
    : 'const' typedef ASSIGN term SEMI
    ;

statement
   : if_else
   | if_op
   | when_op
   | func_call
   | var_def
   | while_op
   | break_op
//   | 'when' paren_expr statement
//   | 'while' paren_expr statement
//   | 'do' statement 'while' paren_expr ';'
   | '{' statement* '}'
   | ret
   | expr ';'
   | ';'
   ;

break_op
    : 'break'
    ;

// Ex. int myNum
type
    : 'int'
    | 'double'
    | 'string'
    | id
    ;

if_op
    : 'if' paren_expr statement
    ;

while_op
    : 'while' paren_expr statement
    ;

when_op
    : 'when' paren_expr statement
    ;

if_else
    :  'if' paren_expr statement else_op
    ;

else_op
    :   'else' statement
    ;

func_def
    : typedef paren_params statement+
    ;

func_call
    : id paren_args
    ;

ret
    : 'return' term ';'
    ;

typedef
    : type id
    | struct_typedef
    ;

struct_typedef
    : 'struct' type id
    ;

paren_params
    : '(' (typedef|(',' typedef))* ')'
    ;

paren_args
    : '(' (term|(',' term))* ')'
    ;

paren_expr
   : '(' expr ')'
   ;

struct_def
   : 'struct' id '{' (typedef ';')+ '}'
   ;

expr
   : comparison
   | sum
//   | term
   ;

comparison
   : sum LESS sum
   | sum GREATER sum
   | sum LESS_EQUAL sum
   | sum GREATER_EQUAL sum
   | sum EQUAL sum
   | sum NOT_EQUAL sum
   | comparison AND comparison
   | comparison OR comparison
   ;

sum
   : number
   | func_call
   | paren_expr
   | id
   | struct_access
//   | typedef
   | sum ARITHMETIC_OPERATOR sum
   ;

term
   : string         // Ex. "yo"
   | struct_paren_arg
//   | typedef
//   | paren_expr     // Ex. ("yo")
   | sum            // Ex. 4 + 4
   | comparison     // Ex. (2 + 2 == 5)
   ;

struct_paren_arg
   : 'struct' id
   ;

BlockComment
    :   '/*' .*? '*/'
        -> skip
    ;

Break: 'break';

LineComment
    :   '//' ~[\r\n]*
        -> skip
    ;

ARITHMETIC_OPERATOR
    : PLUS
    | MINUS
    | MULT
    | DIVIDE
    | MOD
    ;

LESS : '<';
LESS_EQUAL : '<=';
GREATER: '>';
GREATER_EQUAL: '>=';
EQUAL: '==';
NOT_EQUAL: '!=';
AND: '&&';
OR: '||';

PLUS: '+';
MINUS: '-';
MULT: '*';
MOD: '%';
DIVIDE: '/';
ASSIGN: '=';

SEMI: ';';

id
   : ID
   ;

struct_access
   : id '->' id
   ;

number
    : integer
    | type_double
    ;

integer
   : INT
   ;

type_double
   : DOUBLE
   ;

string
   : STRING
   ;

ID
   : [_a-zA-Z]+[_a-zA-Z0-9]*
   | '&'[_a-zA-Z0-9]+
   | [_a-zA-Z]+[_a-zA-Z0-9]*'*'
   ;

STRING
    // Do not allow strings to contain quotation marks
   : '"' ~('\r' | '\n' | '"')* '"'
   ;

CHAR
   : '\''.'\''
   ;


INT
   : [0-9] +
   ;

DOUBLE
   : [0-9]+.[0-9]+
   ;

WS
   : [ \r\n\t] -> skip
;
