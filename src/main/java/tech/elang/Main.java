package tech.elang;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import tech.elang.compiler.ElangCompiler;
import tech.elang.compiler.ElangLexer;
import tech.elang.compiler.ElangParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {
        String inputFile = args[0];

        InputStream is = new FileInputStream(inputFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        ANTLRInputStream input = new ANTLRInputStream(br);
        ElangLexer lexer = new ElangLexer(input);

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        ElangParser parser = new ElangParser(tokens);
        ElangParser.ProgramContext tree = parser.program();
        String out = new ElangCompiler().visitProgram(tree);
        System.out.print(out);

    }
}
