package tech.elang.compiler;

// Generated from elang.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link elangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface elangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link elangParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(elangParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#lang}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLang(elangParser.LangContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#var_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_def(elangParser.Var_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(elangParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(elangParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#if_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_op(elangParser.If_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#if_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_else(elangParser.If_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#func_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_def(elangParser.Func_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#func_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_call(elangParser.Func_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#ret}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRet(elangParser.RetContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#typedef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedef(elangParser.TypedefContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#paren_params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_params(elangParser.Paren_paramsContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#paren_args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_args(elangParser.Paren_argsContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#paren_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen_expr(elangParser.Paren_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(elangParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#comparison}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison(elangParser.ComparisonContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(elangParser.SumContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(elangParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(elangParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(elangParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#integer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(elangParser.IntegerContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#type_double}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_double(elangParser.Type_doubleContext ctx);
	/**
	 * Visit a parse tree produced by {@link elangParser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(elangParser.StringContext ctx);
}