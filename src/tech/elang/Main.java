package tech.elang;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import tech.elang.compiler.elangLexer;
import tech.elang.compiler.elangParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {

        String inputFile = args[0];
        InputStream is = new FileInputStream(inputFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        ANTLRInputStream input = new ANTLRInputStream(br);
        elangLexer lexer = new elangLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        elangParser parser = new elangParser(tokens);
        elangParser.ProgramContext tree = parser.program();

        //String vmcode = new elangVMVisitor().visit(tree);
        //System.out.println(vmcode);

    }
}
